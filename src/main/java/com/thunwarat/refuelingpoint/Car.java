/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.refuelingpoint;

/**
 *
 * @author ACER
 */
public class Car {
    private int x;
    private int fx;
    private int N;
    private char lastDirection = ' ';
    
    public Car(int x,int fx , int N){
        this.x=x;
        this.fx=fx;
        this.N=N;
    }
    public boolean inRoad(int x){
        if(x>=N||x<0){
            return false;
        }
        return true;
    }
    public boolean walk(char direction){
        switch(direction){
            case'A':
                if(!inRoad(x-1)){
                    System.out.println("Stop!!!");
                    return false;
                }
                x=x-1;
                break;
            case'B':
                if(!inRoad(x+1)){
                    System.out.println("Stop!!!");
                    return false;
                }
                x=x+1;
                break;
        }
        lastDirection = direction;
        if(isRefueling()) {
            System.out.println("Refueling point!!!!");
        }
        return true;
    }
    public boolean walk(char direction, int step){
        for(int i = 0;i<step;i++){
            if(!walk(direction)){
                return false;
            }
        }
        return true;
    }
    public boolean walk(){
        return walk(lastDirection);
    }
    
    public boolean walk(int Step){
        return walk(lastDirection,Step);
    }
    
    public boolean isRefueling(){
        if(x == fx){
            return true;
        }
        return false;
    }
    
    @Override
    public String toString(){
         return "Car(" + this.x + ") Fuel(" + this.fx + ")";
    }
}
